import cv2
from patent_core.ILocalRetrieval import MatcherWrapper, IlocalRetrieval
import numpy as np
import time
if __name__ == "__main__":
    model_dir = "/home/zqp/gitlab/models/"
    detector = IlocalRetrieval(model_dir)
    pic1 = "/home/zqp/testpic/imgs/test_img1.jpg"
    pic2 = "/home/zqp/testpic/imgs/test_img2.jpg"

    img1 = cv2.imread(pic1, 0)
    img2 = cv2.imread(pic2, 0)
    start = time.time()
    desc1, kpt1 = detector.extractFeature(img1[..., np.newaxis])
    print("detect1: ", (time.time()-start)*1000, " ms")
    start = time.time()
    desc2, kpt2 = detector.extractFeature(img2[..., np.newaxis])
    print("detect2: ", (time.time()-start)*1000, " ms")

    start = time.time()
    n_inlier, box, match, mask = detector.match(desc1, kpt1, desc2, kpt2)
    print("match: ", (time.time()-start)*1000, " ms")

    img1 = cv2.imread(pic1)
    img2 = cv2.imread(pic2)
    matcher = MatcherWrapper()
    disp = matcher.draw_matches(
        img1, kpt1, img2, kpt2, match, mask)

    cv2.imshow("match", disp)
    cv2.rectangle(img2, (box[0], box[1]), (box[2], box[3]), (0, 255, 0), 3)
    cv2.imshow("im2", img2)
    cv2.waitKey(0)
