import os
import cv2
from patent_core import IPatentFace

def addRectangle(im, boxes):
    for box in boxes:
        cv2.rectangle(im, (box[0], box[1]), (box[2], box[3]), (0, 255, 0), 3)

    return im


if __name__=="__main__":
    model_dir = "/home/zqp/gitlab/models"
    detector = IPatentFace(model_dir)


    pic_dir = "/home/zqp/testpic/lingdaorenlian/"
    for picname in os.listdir(pic_dir):
        if not picname.endswith(".jpg"):
            continue

        im = cv2.imread(pic_dir+picname)

        boxes = detector.detect(im)
        print(picname, 'boxes', boxes)
        addRectangle(im, boxes)
        cv2.imshow("im", im)
        cv2.waitKey(0)





