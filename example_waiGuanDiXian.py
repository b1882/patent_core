from patent_core import IWaiGuanDiXian
import cv2
import os


def addRectangle(im, boxes):
    font = cv2.FONT_HERSHEY_SIMPLEX
    for box in boxes:
        zone = box["zone"]
        cv2.rectangle(im, (zone[0], zone[1]),
                      (zone[2], zone[3]), (0, 0, 255), 2)
        cv2.putText(im, "%s:%.2f" % (
            box["cls"], box["score"]), (zone[0], zone[1]), font, 1, (0, 255, 0))


if __name__ == "__main__":
    model_dir = "/home/zqp/gitlab/models"
    detector = IWaiGuanDiXian(model_dir)

    pic_dir = "/home/zqp/testpic/localretrieval/"

    for pic_name in os.listdir(pic_dir):
        image_path = pic_dir+pic_name
        im = cv2.imread(image_path, cv2.IMREAD_UNCHANGED)

        result = detector.detect(image_path)
        print(result)
        addRectangle(im, result)
        cv2.imshow("im", im)
        cv2.waitKey(0)
