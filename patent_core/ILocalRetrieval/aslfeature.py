import os
import numpy as np
import cv2
import yaml
from .models.feat_model import FeatModel
from .opencvhelper import MatcherWrapper


class AslFeature:
    def __init__(self, model_dir, gpu_id=0) -> None:
        yaml_file = os.path.join(os.path.dirname(
            __file__), "matching_eval.yaml")
        with open(yaml_file, "r") as f:
            self.__config = yaml.load(f, Loader=yaml.FullLoader)

        model_path = os.path.join(model_dir, "aslfeat/model.ckpt-380000")
        self.__net = FeatModel(model_path, **(self.__config["net"]))
        self.__matcher = MatcherWrapper()

    def extractFeature(self, img_gray):
        desc, kpt, _ = self.__net.run_test_data(img_gray)
        return desc, kpt

    def match(self, desc1, kpt1, desc2, kpt2):
        match, mask = self.__matcher.get_matches(
            desc1, desc2, kpt1, kpt2, err_thld=3)
        n_inlier = np.count_nonzero(mask)
        mask1 = mask.reshape([-1]).tolist()
        match1 = [match[idx] for idx, i in enumerate(mask1) if i == 1]

        good_kpts2 = np.array([kpt2[m.trainIdx] for m in match1])
        x1, y1 = np.min(good_kpts2, axis=0).tolist()
        x2, y2 = np.max(good_kpts2, axis=0).tolist()
        x1, y1, x2, y2 = int(x1), int(y1), int(x2), int(y2)

        return n_inlier, [x1, y1, x2, y2], match, mask


if __name__ == "__main__":
    model_dir = "/home/zqp/gitlab/models/"
    detector = AslFeature(model_dir)
    pic1 = "./imgs/test_img1.jpg"
    pic2 = "./imgs/test_img2.jpg"

    img1 = cv2.imread(pic1, 0)
    img2 = cv2.imread(pic2, 0)
    desc1, kpt1 = detector.extractFeature(img1[..., np.newaxis])
    desc2, kpt2 = detector.extractFeature(img2[..., np.newaxis])

    n_inlier, box, match, mask = detector.match(desc1, kpt1, desc2, kpt2)

    img1 = cv2.imread(pic1)
    img2 = cv2.imread(pic2)
    matcher = MatcherWrapper()

    disp = matcher.draw_matches(
        img1, kpt1, img2, kpt2, match, mask)

    cv2.imshow("match", disp)
    cv2.waitKey(0)
