import os
import numpy as np
import torch
from albumentations import (Resize, Normalize, Compose)
from albumentations.pytorch import ToTensor
import cv2
import segmentation_models_pytorch as smp
import math

class IPatentWaiGuanSegment:
    def __init__(self, model_dir, gpu_id=0):
        model_dir += '/' if not model_dir.endswith('/') else ""

        torch.set_default_tensor_type("torch.cuda.FloatTensor")
        model_path = model_dir+'waiguan/waiGuanSeg.pth'
        self.__device = torch.device("cuda:%s" % gpu_id if torch.cuda.is_available() else "cpu")
        self.__net = smp.DeepLabV3('se_resnet101', classes=1, activation=None)
        self.__net.to(self.__device)
        state = torch.load(model_path, map_location=self.__device)
        self.__net.load_state_dict(state["state_dict"])
        self.__net.eval()

        self.__image_transforms = Compose([
            Resize(256, 256),
            Normalize(mean=(0.485, 0.456, 0.406), std=(0.229, 0.224, 0.225), p=1),
            ToTensor(),
        ])
        
    @staticmethod
    def fillHole(mask):
        contours, hierarchy = cv2.findContours(mask, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        drawing = np.zeros_like(mask, np.uint8)  # create a black image
        img_contour = cv2.drawContours(drawing, contours, -1, (255), -1)

        return img_contour

    @staticmethod
    def getBound(mask):
        rows,cols=mask.shape
        for i in range(rows):
            data=mask[i,:]
            up = i  
            if(np.sum(data)>1000):
                break

        for j in range(rows-1,-1,-1):
            data=mask[j,:]
            bottom = j
            if(np.sum(data)>1000):
                break
        
        for k in range(cols):
            data=mask[:,k]
            left = k
            if(np.sum(data)>1000):
                break

        for l in range(cols-1,-1,-1):
            data=mask[:,l]
            right = l
            if(np.sum(data)>1000):
                break

        return up, bottom, left, right

    def imageCut(self, img_bgr, mask):
        originalimage = img_bgr
        h, w = mask.shape

        mask_out = self.fillHole(mask)
        kernel = cv2.getStructuringElement(cv2.MORPH_RECT,(13,13))
        dilate = cv2.dilate(mask_out,kernel,iterations=1)

        contours, _ = cv2.findContours(dilate, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
        c_max = []
        for k in range(len(contours)):
            cnt = contours[k]
            area = cv2.contourArea(cnt)
            rect = cv2.minAreaRect(cnt)  #rect[1][0]<w/10 or rect[1][1]<h/10 width and height of contours
            if area <  h*w/100:  #or rect[1][0]<w/10 or rect[1][1]<h/10)
                continue    
            c_max.append(cnt)

        if len(c_max)<1:
            return False, None

        bgmask = np.zeros(np.shape(dilate), dtype=np.uint8)
        cv2.drawContours(bgmask, c_max, -1, (255), thickness=-1)
        bg = np.ones(np.shape(bgmask), dtype=np.uint8)*255
        masked=cv2.bitwise_not(img_bgr,bg,mask=bgmask)
        masked = cv2.bitwise_not(masked)

        up, bottom, left, right = self.getBound(bgmask)
        width = right - left
        height = bottom - up
        r = math.sqrt(width*width+height*height)

        objectimage = masked[up:bottom,left:right]

        length = r
        bglast = np.ones((int(length),int(length),3), dtype=np.uint8)*255
        pointx = int(r/2- width/2)
        pointy = int(r/2- height/2)
        hcut,wcut,_= objectimage.shape
        bglast[pointy:pointy+hcut,pointx:pointx+wcut] = objectimage

        return True, bglast
        
    def detect(self, img_bgr):
        h, w = img_bgr.shape[:2]
        input=self.__image_transforms(image=img_bgr)["image"]
        input = input[np.newaxis, :].to(self.__device)
        
        with torch.no_grad():
            outputs = self.__net(input)[0]
            
        batch_preds = torch.sigmoid(outputs)  
        
        numpy_output = batch_preds.squeeze(0).detach().cpu().numpy()
        r = np.where(numpy_output > 0.5, 255, 0).astype("uint8")
        mask = cv2.resize(r, (w, h), interpolation=cv2.INTER_CUBIC)
        # ret, result = self.imageCut(img_bgr, mask)
        # return ret, result, mask

        return self.imageCut(img_bgr, mask)

if __name__ == '__main__':
    pic_dir = "/home/zqp/testpic/waiguanseg"
    model_dir = "/home/zqp/gitlab/models/"
    detector = IPatentWaiGuanSegment(model_dir, gpu_id=0)
    for pic_name in os.listdir(pic_dir):
        pic_path = pic_dir + "/" + pic_name
        print(pic_path)

        im = cv2.imread(pic_path)
        ret, result = detector.detect(im)   # ret=False mask error
        if not ret:
            print("mask error")
            cv2.imshow("mask", mask)
        else:
            cv2.imshow("result", result)
        cv2.imshow("im", im)
        cv2.waitKey(0)
