import os
import cv2
from .yolov5 import IObjZoneYOLOV5Detect


class IWaiGuanDiXian:
    def __init__(self, model_dir, gpu_id=0):
        self.__detectors = []
        model_path = model_dir + "/localRetrieval/yolov5localretrieval.pt"
        if os.path.exists(model_path):
            detector = IWaiGuanDiXianCore(
                model_dir + "/localRetrieval", gpu_id)
            self.__detectors.append(detector)

        model_path = model_dir + "/localRetrievalExt/yolov5localretrieval.pt"
        if os.path.exists(model_path):
            detector = IWaiGuanDiXianCore(
                model_dir + "/localRetrievalExt", gpu_id)
            self.__detectors.append(detector)

        model_dir += "/" if not model_dir.endswith("/") else ""
        name_file = model_dir + "localRetrieval/yolov5localretrieval.names"
        self.__names = [name.strip() for name in open(name_file).readlines()]

    def detect(self, image_path, thresh=0.1):
        res = []
        print('len', len(self.__detectors))
        for detector in self.__detectors:
            result = detector.detect(image_path, thresh)
            print('result', result)
            res = res + result
        return res


class IWaiGuanDiXianCore:
    def __init__(self, model_dir, gpu_id=0):
        model_file = model_dir + "/yolov5localretrieval.pt"
        name_file = model_dir + "/yolov5localretrieval.names"
        self.__names = [name.strip() for name in open(name_file).readlines()]
        self.__class_num = len(self.__names)
        self.__detector = IObjZoneYOLOV5Detect(
            model_file, gpu_id)

    def detect(self, image_path, thresh=0.1):
        im = cv2.imread(image_path)
        result = self.__detector.detect(im, thresh)

        for r in result:
            r["cls"] = self.__names[r["cls"]]

        return result


def addRectangle(im, boxes):
    font = cv2.FONT_HERSHEY_SIMPLEX
    for box in boxes:
        zone = box["zone"]
        cv2.rectangle(im, (zone[0], zone[1]),
                      (zone[2], zone[3]), (0, 0, 255), 2)
        cv2.putText(im, "%s:%.2f" % (
            box["cls"], box["score"]), (zone[0], zone[1]), font, 1, (0, 255, 0))


if __name__ == "__main__":
    model_dir = "/home/zqp/gitlab/models"
    detector = IWaiGuanDiXian(model_dir)

    pic_dir = "/home/zqp/testpic/localretrieval/"

    for pic_name in os.listdir(pic_dir):
        image_path = pic_dir+pic_name
        im = cv2.imread(image_path, cv2.IMREAD_UNCHANGED)

        result = detector.detect(image_path)
        print(result)
        addRectangle(im, result)
        cv2.imshow("im", im)
        cv2.waitKey(0)
