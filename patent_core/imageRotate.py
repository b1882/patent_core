import numpy as np
import cv2
import math

from torch._C import dtype


def SVD(points):
    coords = np.array(points)
    mean_coord = np.mean(coords, axis=0)
    coords = coords - mean_coord
    convariance_matrix = np.dot(coords.T, coords)
    eigen_values, eigen_vector = np.linalg.eig(convariance_matrix)
    max_index = np.argmax(np.abs(eigen_values))

    return eigen_vector[max_index]


def rotateImage0(im, angle):
    if len(im.shape) == 3:
        h, w, _ = im.shape
        border_value = (255, 255, 255)
    else:
        h, w = im.shape
        border_value = 255

    cos, sin = math.cos(angle), math.sin(angle)
    rotate_matrix = np.zeros([2, 3], dtype=np.float64)
    rotate_matrix[0, :2] = cos, -sin
    rotate_matrix[1, :2] = sin, cos

    corners = np.matmul(rotate_matrix, np.array([[0, 0, 1], [0, h-1, 1], [w-1, 0, 1], [w-1, h-1, 1]],
                                                dtype=np.float64).transpose())

    point = np.min(corners, axis=1)
    rotate_matrix[:, 2] = -1*point
    nW, nH = (np.max(corners, axis=1) - point+1).astype(np.int32)

    return cv2.warpAffine(im, rotate_matrix, (nW, nH), borderValue=border_value)


def getRotateAngle(eigen_vector):
    angle = math.acos(
        np.dot(eigen_vector, np.array([1, 0], dtype=np.float32)))

    if eigen_vector[1] > 0:
        angle *= -1
    return -1*angle


def rotateImage(im):
    if len(im.shape) == 3:
        im_gray = cv2.cvtColor(im, cv2.COLOR_BGR2GRAY)
    elif len(im.shape) == 1:
        im_gray = im
    else:
        return im
    thread, im_binary = cv2.threshold(im_gray, 0, 255, cv2.THRESH_OTSU)
    ret = np.where(im_binary == 0)
    num_coords = len(ret[0])
    coords = np.zeros((num_coords, 2), dtype=np.float32)
    coords[:, 0] = ret[1]
    coords[:, 1] = ret[0]

    vector = SVD(coords)
    angle = getRotateAngle(vector)
    # print("angle: ", angle*180/math.pi)

    im_rotate = rotateImage0(im_gray, angle)
    thread, im_binary = cv2.threshold(im_rotate, 0, 255, cv2.THRESH_OTSU)
    ys, xs = np.where(im_binary == 0)
    mean_x = (np.max(xs) + np.min(xs))/2
    mean_y = (np.max(ys) + np.min(ys))/2

    num_less_x = len(np.where(xs < mean_x)[0])
    num_more_x = len(np.where(xs > mean_x)[0])
    num_less_y = len(np.where(ys < mean_y)[0])
    num_more_y = len(np.where(ys > mean_y)[0])

    # print("num_less_x: ", num_less_x, "\tnum_more_x: ", num_more_x)
    # print("num_less_y: ", num_less_y, "\tnum_more_y: ", num_more_y)

    im_rotate = rotateImage0(im, angle)
    # cv2.imshow("src2", im_rotate)
    if num_less_x > num_more_x:
        if num_less_y > num_more_y or num_less_x/num_more_x > num_more_y/num_less_y:
            im_rotate = rotateImage0(im_rotate, math.pi)
    else:
        if num_less_y > num_more_y and num_less_y/num_more_y > num_more_x/num_less_x:
            im_rotate = rotateImage0(im_rotate, math.pi)

    return im_rotate
