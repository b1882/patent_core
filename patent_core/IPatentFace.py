from .IPatentLingDaoRenFaceType import IPatentLingDaoRenFaceTypeClassifier
from .IFaceDetect import IFaceZoneDetect
from PIL import Image
import os
import cv2

class IPatentFace:
    def __init__(self, model_dir, gpu_id=0):
        model_dir += '/' if not model_dir.endswith('/') else ""
        self.detector = IFaceZoneDetect(model_dir, gpu_id)
        self.classifier = IPatentLingDaoRenFaceTypeClassifier(model_dir, gpu_id)

    def getAlignFace(self, im, box, points):
        return self.detector.get_align_face(im, box, points)

    @staticmethod
    def getPadBox(box, im_w, im_h):
        pad_scale = 0.15
        w = box[2] - box[0]
        h = box[3] - box[1]
        pad_w = w * pad_scale
        pad_h = h * pad_scale

        x1 = int(box[0] - pad_w) if (box[0] - pad_w)>0 else 0
        y1 = int(box[1] - pad_h) if (box[1] - pad_h)>0 else 0
        x2 = int(box[2] + pad_w) if (box[2] + pad_w)<im_w else im_w
        y2 = int(box[3] + pad_h) if (box[3] +pad_h)<im_h else im_h

        return x1, y1, x2, y2

    def detect(self, im, confidence=0.7):
        boxes = self.detector.detect(im, confidence)

        for box in boxes:
            x1, y1, x2, y2, score = box
            im_roi = cv2.cvtColor(im[y1: y2, x1: x2], cv2.COLOR_BGR2RGB)
            img = Image.fromarray(im_roi).convert("RGB")
            result = self.classifier.classify(img)

            box.append(result["category"])

        return boxes

if __name__=="__main__":
    model_dir = "/root/models"
    detector = IPatentFace(model_dir)


    pic_dir = "/root/test1/"
    for picname in os.listdir(pic_dir):
        if not picname.endswith(".jpg"):
            continue

        im = cv2.imread(pic_dir+picname)

        boxes,points = detector.detect(im)

        print(picname, 'boxes', boxes, 'points', points )
        assert (len(boxes) == len(points))
        for i in range(len(boxes)):
            im_temp, ret = detector.getAlignFace(im,boxes[i],points[i])
            if ret == False:
                print("align false")
                continue
            pic_save_path = picname+"_test"
            cv2.imwrite(pic_save_path+"%s.jpg"%i,im_temp)

        # im_tmp1 = drawBoxesAndPoints(im,boxes,points)




