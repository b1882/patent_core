import cv2
import os
import faiss
import numpy as np
import torch
import pickle
from .IPatentFuTuFeatureExtract import IPatentFuTuFeatureExtract
from .re_ranking import re_rank


class IPatentFuTuRetrieval:
    def __init__(self, model_dir, gpu_id=0):
        self.__extracter = IPatentFuTuFeatureExtract(model_dir, gpu_id)
        self.__feature_len = self.__extracter.feature_len

    def create_retrieval_model(self, retrieval_model_path):
        assert (os.path.exists(retrieval_model_path))

        retrieval_model = faiss.read_index(retrieval_model_path)
        return retrieval_model

    def extractFeature(self, img_bgr):
        try:
            feature = self.__extracter.extractFeature(img_bgr)
        except:
            return []

        return feature

    def buildRetrievalDatabase(self, features, retrieval_model_path):
        if os.path.exists(retrieval_model_path):
            retrieval_model = faiss.read_index(retrieval_model_path)
        else:
            retrieval_model = faiss.IndexFlatL2(self.__feature_len)

        features = np.array(features)
        retrieval_model.add(features)
        faiss.write_index(retrieval_model, retrieval_model_path)

        return retrieval_model

    def query(self, im, k, retrieval_model):
        feature = self.extractFeature(im)

        feature = np.array([feature])
        res = retrieval_model.search(feature, k)
        return res[1][0], 1-res[0][0]/2

    @staticmethod
    def secondQuery(query_feature, gallery_features):
        q = torch.from_numpy(np.array([query_feature]))
        g = torch.from_numpy(gallery_features)
        distance = re_rank(q, g).flatten()
        distance_index = distance.argsort()
        distance = np.sort(distance)

        return distance_index, 1 - distance

        pass

    # @staticmethod
    # def secondQuery(features,k=30):
    #     features = np.array(features)
    #     assert len(features.shape)==2
    #     assert k<=features.shape[0] and k>0
    #
    #     feature = np.mean(features[:k,:],axis=0)
    #     features -= feature
    #     distance = np.sqrt(np.sum(features*features,axis=1))
    #     index = np.argsort(distance)
    #     sort_distance = distance[index]
    #
    #     return list(zip(index,sort_distance))


def buildRetrievalDatabase():
    model_dir = "/home/zqp/gitlab/models"
    net = IPatentFuTuRetrieval(model_dir)

    features = []
    index = 0
    # pic_dir = "/home/zqp/testpic/patentRetrieval/"
    pic_dir = "/media/zqp/data/data/patent/电缆原图/"

    pic_paths = sorted(
        [pic_dir+pic_name for pic_name in os.listdir(pic_dir) if pic_name.endswith(".jpg")])
    num = len(os.listdir(pic_dir))

    # cv2.namedWindow("src1", 0)
    # cv2.namedWindow("src2", 0)
    # cv2.namedWindow("src3", 0)
    for pic_path in pic_paths:
        im = cv2.imread(pic_path)
        res = net.extractFeature(im)

        # cv2.waitKey(0)
        if len(res):
            features.append(res)

        index += 1
        print("processed***************%s/%s" % (index, num))

    feature = features[2] - features[3]
    distance = np.sum(np.dot(feature, feature))

    for idx in range(1024):
        print(features[0][idx], "\t", features[1][idx])

    print("d01: ", distance)

    f = open("features.pkl", "wb")
    pickle.dump(features, f)

    net.buildRetrievalDatabase(features, "./123.index")


def query():
    model_dir = "/home/zqp/gitlab/models"
    retrieval_model_path = "./123.index"
    net = IPatentFuTuRetrieval(model_dir)
    retrieval_model = net.create_retrieval_model(retrieval_model_path)
    features = np.array(pickle.load(open("features.pkl", "rb")))

    cv2.namedWindow("base", 0)
    cv2.namedWindow("query", 0)
    pic_dir = "/home/zqp/testpic/futuseg/"
    pic_paths = sorted(
        [pic_dir+pic_name for pic_name in os.listdir(pic_dir) if pic_name.endswith(".jpg")])
    for pic_name in os.listdir(pic_dir):
        print(pic_name)
        im = cv2.imread(os.path.join(pic_dir, pic_name))

        cv2.imshow("base", im)

        res = net.query(im, 50, retrieval_model)
        query_feature = net.extractFeature(im)
        gallery_features = features[res[0]]
        indexs, confidence = net.secondQuery(query_feature, gallery_features)
        print(res)

        top = 0
        for idx in range(len(indexs)):
            im_query = cv2.imread(pic_paths[res[0][indexs[idx]]])
            cv2.imshow("query", im_query)
            top += 1

            print("top*********", top, confidence[idx])
            cv2.waitKey(0)


if __name__ == "__main__":
    buildRetrievalDatabase()
    # query()
