import torch
from ResNet import ResNet50
from torchvision import transforms
import os
import cv2
from PIL import Image, ImageFont, ImageDraw
import torch.nn.functional as F

class IPatentWaiGuanTypeClassifier:
    def __init__(self, model_dir, gpu_id=0):
        model_dir += '/' if not model_dir.endswith('/') else model_dir

        labelfile = model_dir+'waiguan/waiGuanType.txt'
        assert(labelfile)
        self.__label = [line.strip() for line in open(labelfile).readlines()]

        self.__device = torch.device("cuda:%s" % gpu_id if torch.cuda.is_available() else "cpu")

        self.__net = ResNet50(len(self.__label)).resnet50model()
        self.__net.to(self.__device)

        model_path = model_dir + "waiguan/waiGuanType.pth"
        tmp = torch.load(model_path, map_location=self.__device)
        self.__net.load_state_dict(tmp)
        self.__net.eval()

        self.__image_transforms = transforms.Compose([
            transforms.Resize((224,224)),
            transforms.ToTensor(),
        ])

    def classify(self, image):
        input=self.__image_transforms(image)
        img = input.unsqueeze(0)

        with torch.no_grad():
            output, feature = self.__net(img.cuda(self.__device))[0]
            output = F.softmax(output, dim=0).cpu()
            pred = torch.argmax(output)

            return {"category": self.__label[pred], "score": output.numpy()[pred]}

def run():
    modelDir = r'/home/zqp/gitlab/models'
    classifier = IPatentWaiGuanTypeClassifier(modelDir,0)

    picDir = r'/home/zqp/testpic/liangdaorenlian/'
    for picName in os.listdir(picDir):
        im = cv2.imread(picDir+picName)
        img = Image.open(picDir+picName).convert('RGB')
        result = classifier.classify(img)
        print(result)

        cv2.imshow("im", im)
        cv2.waitKey(0)

if __name__ == '__main__':
    run()
