import os
import cv2
import time
from .yolov5 import IObjZoneYOLOV5Detect


class IMinorityLanguage:
    def __init__(self, model_dir, gpu_id=0):
        model_dir = model_dir + \
            "/" if not model_dir.endswith("/") else model_dir
        model_file = model_dir + "patent/yolov5patentText.pt"
        self.__net = IObjZoneYOLOV5Detect(model_file, gpu_id)

        self.__names = ["arabic", "zang", "korea", "japan",
                        "meng", "uighur", "russian", "chinese", "xiwen"]

    def detect(self, image_path, thresh=0.45):
        if not os.path.exists(image_path):
            return []

        im = cv2.imread(image_path)
        boxes = self.__net.detect(im, thresh)

        for box in boxes:
            box["cls"] = self.__names[box["cls"]]

        return boxes


def addRectangle(im, boxes):
    font = cv2.FONT_HERSHEY_SIMPLEX
    for box in boxes:
        zone = box["zone"]
        cv2.rectangle(im, (zone[0], zone[1]),
                      (zone[2], zone[3]), (0, 0, 255), 2)
        cv2.putText(im, "%s:%.2f" % (
            box["cls"], box["score"]), (zone[0], zone[1]), font, 1, (0, 255, 0))


if __name__ == "__main__":
    model_dir = "/home/zqp/gitlab/models/"
    detector = IMinorityLanguage(model_dir)
    picdir = "/root/testpic/patentText/"
    result_dir = "./result/"
    if not os.path.exists(result_dir):
        os.mkdir(result_dir)
    pic_names = os.listdir(picdir)

    for picname in pic_names:
        print(picname)
        if not picname.endswith(".jpg"):
            continue

        start = time.time()
        boxes = detector.detect(picdir + picname)
        end = time.time()

        im = cv2.imread(picdir+picname)
        addRectangle(im, boxes)

        cv2.imwrite(result_dir+picname, im)
        print(boxes)
        print("detect cost time %s ms" % ((end - start) * 1000))
