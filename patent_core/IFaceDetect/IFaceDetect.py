import os
import cv2
import numpy as np
import torch
import torchvision.models.resnet as resnet
from .torchvision_model import RetinaFace
import torch.nn.functional as F
import torchvision.ops as ops


class IFaceZoneDetect:
    def __init__(self, model_dir, gpu_id=0):
        model_dir = model_dir + \
            '/' if not model_dir.endswith('/') else model_dir

        self.__device = torch.device(
            "cuda:%s" % gpu_id if torch.cuda.is_available() else "cpu")

        model_path = model_dir+"patent/patentFaceDetect.pth"
        assert(model_path)
        return_layers = {'layer2': 1, 'layer3': 2, 'layer4': 3}
        backbone = resnet.__dict__["resnet50"](pretrained=True)
        for name, parameter in backbone.named_parameters():
            if name == 'conv1.weight':
                parameter.requires_grad_(False)

        self.__net = RetinaFace(backbone, return_layers, anchor_nums=3)

        retina_dict = self.__net.state_dict()
        pre_state_dict = torch.load(
            model_path, map_location=self.__device)
        pretrained_dict = {k[7:]: v for k,
                           v in pre_state_dict.items() if k[7:] in retina_dict}
        self.__net.load_state_dict(pretrained_dict)

        self.__net = self.__net.to(self.__device)
        self.__net.eval()

    def py_cpu_nms(self, dets, thresh):
        x1 = dets[:, 0]
        y1 = dets[:, 1]
        x2 = dets[:, 2]
        y2 = dets[:, 3]
        scores = dets[:, 4]
        areas = (x2 - x1 + 1) * (y2 - y1 + 1)
        order = scores.argsort()[::-1]
        keep = []
        while order.size > 0:
            i = order[0]
            keep.append(i)
            xx1 = np.maximum(x1[i], x1[order[1:]])
            yy1 = np.maximum(y1[i], y1[order[1:]])
            xx2 = np.minimum(x2[i], x2[order[1:]])
            yy2 = np.minimum(y2[i], y2[order[1:]])
            w = np.maximum(0.0, xx2 - xx1 + 1)
            h = np.maximum(0.0, yy2 - yy1 + 1)
            inter = w * h
            ovr = inter / (areas[i] + areas[order[1:]] - inter)
            inds = np.where(ovr <= thresh)[0]
            order = order[inds + 1]
        return keep

    @staticmethod
    def resize(image, size):
        image = F.interpolate(image.unsqueeze(
            0), size=size, mode="nearest").squeeze(0)
        return image

    def forward(self, input_img, confidence):
        with torch.no_grad():
            classifications, bboxes, landmarks = self.__net(input_img)
            batch_size = classifications.shape[0]
            picked_boxes = []
            picked_landmarks = []
            picked_scores = []

            for i in range(batch_size):
                classification = torch.exp(classifications[i, :, :])
                bbox = bboxes[i, :, :]
                landmark = landmarks[i, :, :]

                # choose positive and scores > score_threshold
                scores, argmax = torch.max(classification, dim=1)
                argmax_indice = argmax == 0
                scores_indice = scores > confidence
                positive_indices = argmax_indice & scores_indice

                scores = scores[positive_indices]

                if scores.shape[0] == 0:
                    # picked_boxes.append(None)
                    # picked_landmarks.append(None)
                    # picked_scores.append(None)
                    continue

                bbox = bbox[positive_indices]
                landmark = landmark[positive_indices]

                keep = ops.boxes.nms(bbox, scores, 0.3)
                keep_boxes = bbox[keep]
                keep_landmarks = landmark[keep]
                keep_scores = scores[keep]
                keep_scores.unsqueeze_(1)
                picked_boxes.append(keep_boxes.cpu().numpy())
                picked_landmarks.append(keep_landmarks.cpu().numpy())
                picked_scores.append(keep_scores.cpu().numpy())

            return picked_boxes, picked_landmarks, picked_scores

    def detect(self, im, confidence=0.5):
        img = torch.from_numpy(im)
        img = img.permute(2, 0, 1)
        # scale = 4
        # size1 = int(img.shape[1]/scale)
        # size2 = int(img.shape[2]/scale)
        # img = self.resize(img.float(), (size1, size2))
        input_img = img.unsqueeze(0).float().cuda()
        boxes, landmarks, scores = self.forward(input_img, confidence)
        if len(boxes) == 0:
            return []

        result = []
        scores = scores[0].reshape(-1).tolist()
        boxes = boxes[0].tolist()
        for box, score in zip(boxes, scores):
            x1, y1, x2, y2 = list(box)
            box = list(map(int, box))
            h, w = y2-y1, x2-x1
            pad_w, pad_h = (x2-x1)*0.3, (y2-y1)*0.3

            x1 = max(int(x1-pad_w), 0)
            y1 = max(int(y1-pad_h), 0)
            x2 = min(int(x2+pad_w), im.shape[1])
            y2 = min(int(y2+pad_h), im.shape[0])

            result.append([x1, y1, x2, y2, score])

        return result


def drawBoxes(im, boxes):
    for box in boxes:
        cv2.rectangle(im, (box[0], box[1]), (box[2], box[3]), (0, 255, 0), 3)

    return im


if __name__ == "__main__":
    model_dir = "/home/zqp/gitlab/models"
    detector = IFaceZoneDetect(model_dir)

    # pic_dir = "/home/zqp/testpic/testimage/"
    pic_dir = "/home/zqp/testpic/lingdaorenlian/"
    cv2.namedWindow("im", 0)
    for picname in os.listdir(pic_dir):
        if not picname.endswith(".jpg"):
            continue

        im = cv2.imread(pic_dir+picname)
        print(pic_dir+picname)

        boxes = detector.detect(im)

        print(picname, 'boxes', boxes)

        im_tmp1 = drawBoxes(im, boxes)
        cv2.imshow("im", im_tmp1)
        cv2.waitKey(0)
