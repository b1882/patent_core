import os
import cv2
import darknet

class IObjZoneYOLOV3Detect:
    def __init__(self, cfg_file, weight_file, class_num, gpu_id=0):
        self.__class_num = class_num
        self.__detector = darknet.load_net(bytes(cfg_file, encoding="utf8"), bytes(weight_file, encoding="utf8"), gpu_id)

    @staticmethod
    def boxConvert(b, w, h):
        x1 = b[0] - b[2]/2
        y1 = b[1] - b[3]/2
        x2 = x1 + b[2]
        y2 = y1 + b[3]

        x1 = 0 if x1<0 else int(x1)
        y1 = 0 if y1<0 else int(y1)
        x2 = w if x2>w else int(x2)
        y2 = h if y2>h else int(y2)

        return (x1, y1, x2, y2)

    def detect(self, image_path, thresh = 0.7):
        result = darknet.detect(self.__detector, self.__class_num, bytes(image_path, encoding="utf8"), thresh)
        r = []
        h, w = cv2.imread(image_path, cv2.IMREAD_UNCHANGED).shape[:2]
        for tmp in result:
            temp = {}

            temp["zone"] = self.boxConvert(tmp[2], w, h)
            temp["cls"] = tmp[0]
            temp["score"] = tmp[1]
            r.append(temp)
        return r

def addRectangle(im,boxes):
    font = cv2.FONT_HERSHEY_SIMPLEX
    for box in boxes:
        zone = box["zone"]
        cv2.rectangle(im,(zone[0],zone[1]),(zone[2],zone[3]),(0,0,255),2)
        cv2.putText(im, "%s:%.2f"%(box["cls"],box["score"]),(zone[0],zone[1]),font,1,(0,255,0))


def run():
    cfg_file = "/home/zqp/gitlab/models/patent/yolov3patent.cfg"
    weight_file = "/home/zqp/gitlab/models/patent/yolov3patent.weights"
    model_dir = "/home/zqp/gitlab/models/"

    picdir = "/media/zqp/data/data/patent/VOCdevkit/VOC2007/JPEGImages/"
    for picname in os.listdir(picdir):
        im = cv2.imread(picdir+picname)
        cv2.imshow("im", im)
        if cv2.waitKey(0)==27:
            break

if __name__=="__main__":
    run()


