import cv2
import os
import numpy as np
from patent_core.IPatentWaiGuanRetrieval import IPatentWaiGuanRetrieval
import pickle


def buildRetrievalDatabase():
    model_dir = "/home/zqp/gitlab/models"
    net = IPatentWaiGuanRetrieval(model_dir)

    features = []
    index = 0
    pic_dir = "/home/zqp/testpic/waiguanseg/"

    pic_paths = sorted(
        [pic_dir+pic_name for pic_name in os.listdir(pic_dir) if pic_name.endswith(".jpg")])
    num = len(os.listdir(pic_dir))

    # cv2.namedWindow("im_resize", 0)
    for pic_path in pic_paths:
        im = cv2.imread(pic_path)
        res = net.extractFeature(im)

        # cv2.waitKey(0)
        if len(res):
            features.append(res)

        index += 1
        print("processed***************%s/%s" % (index, num))

    feature = features[2] - features[3]
    distance = np.sum(np.dot(feature, feature))

    for idx in range(1024):
        print(features[0][idx], "\t", features[1][idx])

    print("d01: ", distance)

    f = open("features.pkl", "wb")
    pickle.dump(features, f)

    net.buildRetrievalDatabase(features, "./123.index")


def query():
    model_dir = "/home/zqp/gitlab/models"
    retrieval_model_path = "./123.index"
    net = IPatentWaiGuanRetrieval(model_dir)
    retrieval_model = net.create_retrieval_model(retrieval_model_path)
    features = np.array(pickle.load(open("features.pkl", "rb")))

    cv2.namedWindow("base", 0)
    cv2.namedWindow("query", 0)
    pic_dir = "/home/zqp/testpic/waiguanseg/"
    pic_paths = sorted(
        [pic_dir+pic_name for pic_name in os.listdir(pic_dir) if pic_name.endswith(".jpg")])
    for pic_name in os.listdir(pic_dir):
        print(pic_name)
        im = cv2.imread(os.path.join(pic_dir, pic_name))

        cv2.imshow("base", im)

        res = net.query(im, 50, retrieval_model)
        query_feature = net.extractFeature(im)
        gallery_features = features[res[0]]
        indexs, confidence = net.secondQuery(query_feature, gallery_features)
        print(res)

        top = 0
        for idx in range(len(indexs)):
            im_query = cv2.imread(pic_paths[res[0][indexs[idx]]])
            cv2.imshow("query", im_query)
            top += 1

            print("top*********", top, confidence[idx])
            cv2.waitKey(0)


if __name__ == "__main__":
    buildRetrievalDatabase()
    query()
